//
//  ViewController.swift
//  anasco-cesar-ios-2018-b
//
//  Created by Washington Añasco on 27/11/18.
//  Copyright © 2018 Washington Añasco. All rights reserved.
//

import UIKit
import FirebaseAuth

class ViewController: UIViewController {

    @IBOutlet weak var correoElectronicoCampoTexto: UITextField!
    
    @IBOutlet weak var claveCampoTexto: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func botonIngresar(_ sender: Any) {
        
        //correo:   cesar@hotmail.com
        //clave:    hola1234
        
        
        let correo = correoElectronicoCampoTexto.text!
        let clave = claveCampoTexto.text!
        
        Auth.auth().signIn(withEmail: correo, password: clave){
            (data, error) in
            if let error = error {
                print(error)
                return
            }
            //print("Welcome...!!!")
            // Ejecuta la transicion, con el self se hace referencia a la clase principal
            self.performSegue(withIdentifier: "loginSegue", sender: self)
        }
        
        
    }
    
    
    
}

