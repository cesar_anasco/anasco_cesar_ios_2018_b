//
//  SecViewController.swift
//  anasco-cesar-ios-2018-b
//
//  Created by Washington Añasco on 27/11/18.
//  Copyright © 2018 Washington Añasco. All rights reserved.
//

import UIKit

class SecViewController: UIViewController {
    
    @IBOutlet weak var campoNumero: UITextField!
    @IBOutlet weak var numerosPares: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    @IBAction func calcularNumerosPares(_ sender: Any) {
        
        let n = Int(campoNumero.text!) ?? 0
        var miArray = [0]
        print(n)
        
        //El 0 no es numero par
        
        for i in 1...n*2
        {
            if i % 2 != 0
            {
                
            } else {
                
                miArray.append(i)
            }
        }
        numerosPares.text = "\(miArray)"
    
    }
    @IBAction func regresarInicio(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
